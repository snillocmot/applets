var fs = require('fs');
var csvWriter = require('csv-write-stream')


function saveLine (file, text) {
   // var datetime = '[' + getDateTime() + '] ';
   // var text = datetime + command + '\r\n';
    fs.appendFile(file, text + "\n", function (err) {
        if (err) return console.log(err);
        console.log('successfully appended "' + text + '"');
    });
}


function readLines(input, func) {
  var remaining = '';

  input.on('data', function(data) {
    remaining += data;
    var index = remaining.indexOf('\n');
    while (index > -1) {
      var line = remaining.substring(0, index);
      remaining = remaining.substring(index + 1);
      func(line);
      index = remaining.indexOf('\n');

    }
  });

  input.on('end', function() {
    if (remaining.length > 0) {
      func(remaining);
    }
  });
}

function func(data) {

  obj = JSON.parse(data)
  csvLine = obj.time + "," +obj.value
  console.log(csvLine)

  saveLine("out.csv", csvLine)


}

var input = fs.createReadStream('src/samples/49.json');
readLines(input, func);