# Applet service doc's

Applet service is a process running service for back-end logic to support distributed applications.

Applets can easily be added, paused, configured and removed on the fly.

Each applet can run a number of different modules which operate as black boxes with data input, controls and outputs. The configuration makes it unique for a customer need.


	Service
		 |------------------------------------------------------------
		 |				     	  |				 						  |  			|
		ISO10816  		 Configurable threshold alert           Pattern detection 
		   |
		   |- Statistics
		   |- 10861 threshold
		   |- Aggregation
		   |- Notification
		   |- Configuration 


module type (ISOQE)
I-aggregates
S-what data needs to be edited, added or cleaned?
O-Do something smart
Q-Prepare for next to consume (Style format, change resolution)
E-End user push interface

		         
res = ISO10816(Customer auth+id, inputs, configuration, outputs)
res.appPage
res.configPage

applet configuration
-modules
	module configuration 

## Applet API

This API provides functions to interact with the overall service. It tells you what apps are running and statistics on each.

It also provides a way to list and edit the configuration

## App API

## Instansiating applets
Applets can be run in different instances, this means an applet can be run with different configurations. This means it's possible for different customer and different domain to to use the same services, with their own containers and processes. 


###  Search data
- A-B time window
- Above average
- Below average



### Data annotation tool 
- List all data
	- What has been reviewed?
	- What has be classified?
	- What hasn't been reviewed?
	- What hasn't been classified?
- Let me load a window from A-B 
- Let me mark window as read
- Let me describe from point A-B
- Let me export data 




