/*

  Main running point of the application, responsible for:
  - Setting up HTTP API
  - Serving HTTP files
  - Starting the applet service

*/

device = {
    serial: String,
    secret: String,
    type: String,
    comments: [{ body: String, date: Date }],
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    meta: {
        datapoints: Number,
    }
}

crud = require('crud');

crud.setup({
  "base" : "api/"
}, app);


crud.init("device", device)

crud.doc()