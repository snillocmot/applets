var http = require("http");

var express = require('express');
global.bodyParser = require('body-parser');
global.app = express()
global.fs = require('fs')
config = JSON.parse(fs.readFileSync('package.json', 'utf8')).config;
applet = require('applet');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(__dirname + "/src/", {}));
port = process.env.PORT || 9120
app.listen(port);
global.app = app;
//crud = require('crud');
//crud.setup({}, app);
console.log(" # Serving up HTTP files from: " + __dirname + "/src/");
require('dns').lookup(require('os').hostname(), function(err, add, fam) {
    //if(add != undefined){
    console.log(add ? ' # address: ' + add + ":" + port : '# Port: ' + port);
    //}
})

//If we're in prod' keep up the MQTT port
if(process.env.PORT){
  setInterval(function() {
      http.get("http://shielded-everglades-55050.herokuapp.com");
  },  Math.floor((Math.random() * 250) + 100) * 1000); 
}

/*    

  GET /api    

  Lists details about the applet app.

  What time was the app started?
  What is the runtime ticks?
  What individual modules are enabled?
  What instances are running and what modules are in each?
  How many total mqtt connections have been made?
  What's the total bandwidth is being used?

*/
global.suuid = config.suuid;
app.get("/api/", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    body = {}
    body.status = applet.status
    body.started = applet.started
    body.ticks = applet.ticks
    body.instances = []
    instanceList = Object.keys(applet.instances)
    for (var i = 0; i < instanceList.length; i++) {
        instance = applet.instances[instanceList[i]]
        actualName = instanceList[i].split("-")[1]
        //console.log(instance.db);
        detail = {}
        detail.name = actualName
        //    detail.details =  instance.db
        detail.config = instance.config
        body.instances.push(detail)
    }
    res.send(body);
});
/*    

  GET /stop    

  Stops all instances which are running

*/
app.get("/api/" + suuid + "/stop", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    applet.stop()
    console.log("stopping")
    res.send(JSON.stringify(applet.status));
});
app.get("/api/" + suuid + "/start", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    applet.start()
    console.log("starting")
    res.send(JSON.stringify(applet.status));
});
app.get("/api/" + suuid + "/logs", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(applet.logs));
});
app.get("/api/" + suuid + "/config", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(applet.config));
});
app.get("/samples", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    fz = []
    const fs = require('fs');
    fs.readdir("src/samples", (err, files) => {
        files.forEach(file => {
            fz.push(file);
        });
        res.send(JSON.stringify(fz));
    })
});
/*
 *  GET /api/statistics
 *  
 *
 */
app.get("/api/config", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(applet.config));
});
/*
    Module details
*/
app.get("/api/module/statistics", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    body = applet.procModule["applet-statistics"].db
    res.send(JSON.stringify(body));
});