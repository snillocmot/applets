  resolutions = [
      ["second", 5],
      ["second", 10],
      ["second", 15],
      ["second", 30],
      ["minute", 1],
      ["minute", 2],
      ["minute", 5],
      ["minute", 10],
      ["minute", 15],
      ["minute", 30],
      ["minute", 45],
      ["hour", 1],
      ["hour", 2],
      ["hour", 3],
      ["hour", 4],
      ["hour", 6],
      ["hour", 8],
      ["hour", 12],
      ["day", 1],
      ["week", 1],
      ["month", 1]
  ]
  options = {
      chart: {
          zoomType: 'x',
          /*  events: {
                selection: selectPointsByDrag,
                selectedpoints: selectedPoints,
                click: unselectByClick
            },*/
      },
      turboThreshold: 100000,
      title: {
          text: title
      },
      subtitle: {
          text: subtitle
      },
      rangeSelector: {
          buttons: [{
              type: 'hour',
              count: 24,
              text: 'Use the range selector above!',
              dataGrouping: {
                  forced: true,
                  units: [
                      ['second', [1]]
                  ]
              }
          }]
      },
      xAxis: {
          type: 'datetime',
          title: {
              text: xTitle
          },
          /*events: {
              setExtremes: function(e) {
                  windowSizeMin = Highcharts.dateFormat(null, e.min)
                  windowSizeMax = Highcharts.dateFormat(null, e.max)
                  windowSizeTrigger = e.trigger;
              }
          }*/
      },
      yAxis: {
          title: {
              text: yTitle
          }
      },
      legend: {
          enabled: false
      },
      series: []
  }
  /*
   * Input is a JS array of .json formatted end points
   */
  function loadChart(sourcesArray) {
      console.log("charter, loading")
      seriesCounter = 0;
      var series = []
      sourcesArray.forEach(function(sourceUrl) {
          console.log("Attempting to GET data from: " + sourceUrl)

          $.get(sourceUrl, function(data) {
              // If it's not a CSV file we assume it's JSON body
              if (!sourceUrl.indexOf(".csv") > -1) {
                  series[seriesCounter] = {}
                  series[seriesCounter].name = sourceUrl.split("/")[5]
                  series[seriesCounter].data = []
                  $.each(data, function(key, obj) {
                      obj.value = parseFloat(obj.value);
                      line = [new Date(obj.time).getTime(), obj.value]
                      series[seriesCounter].data.push(line)
                  });
                  if (series[seriesCounter].data.length == 0) {
                      alert("No data found for  " + series[seriesCounter].name)
                  }
                  if (chart && chart.series[seriesCounter]) {
                      console.log("Redrawing with " + series[seriesCounter].data.length + "data points")
                      chart.series[seriesCounter].setData(series[seriesCounter].data, true);
                      chart.redraw()
                  }
                  seriesCounter = seriesCounter + 1;
              } else {
                  var lines = []
                  try {
                      lines = data.split('\n');
                  } catch (e) {
                      try {
                          JSON.parse(data);
                          console.log("Is pure json")
                      } catch (e) {
                          console.log("Not pure json")
                          headers = String("DateTime, number")
                          lines.push(headers)
                          $.each(data, function(key, val) {
                              //obj = JSON.parse(val)
                              obj = val
                              line = String(obj.time + ",    " + obj.value)
                              lines.push(line)
                          });
                      }
                  }
                  /* Configure axis data types and type of the chart */
                  xformat = ""
                  var headers = []
                  $.each(lines, function(lineNo, line) {
                      var items = line.split(',');
                      if (lineNo == 0) {
                          headers = items
                          $.each(headers, function(itemNo, item) {
                              if (itemNo == 0) {
                                  if (item == "DateTime" || item == "epoch") {
                                      xformat = item
                                      options.xAxis.type = "datetime"
                                  } else if (item == "category") {
                                      xformat = item
                                      //options.xAxis.type = "category"
                                  }
                              }
                              if (itemNo == 0) {
                                  if (item == "count") {
                                      xformat = item
                                  }
                              }
                              series[itemNo] = {
                                  type: p,
                                  name: item,
                                  data: []
                              }
                          });
                      } else {
                          $.each(items, function(itemNo, item) {
                              if (itemNo == 0) {
                                  // Sort out X axis labels and intervals
                                  label = ""
                                  if (xformat == "DateTime") {
                                      dateObj = new Date(item)
                                      var month = dateObj.getUTCMonth() + 1;
                                      var day = dateObj.getUTCDate();
                                      var year = dateObj.getUTCFullYear();
                                      var hour = dateObj.getUTCHours();
                                      var minute = dateObj.getUTCMinutes();
                                      var seconds = dateObj.getUTCSeconds();
                                      newdate = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + seconds;
                                      label = newdate
                                  } else if (xformat == "epoch") {
                                      label = new Date(item * 1000)
                                      var month = dateObj.getUTCMonth() + 1;
                                      var day = dateObj.getUTCDate();
                                      var year = dateObj.getUTCFullYear();
                                      newdate = day + "/" + month + "/" + year;
                                      label = newdate
                                  } else if (xformat == "count") {
                                      label = item
                                  }
                                  if (xformat == "category") {
                                      label = item
                                  } else {
                                      console.log(label + "Not pushed to x label...")
                                      //options.xAxis.categories.push(label);
                                  }
                              } else {
                                  if (type == "pie") {
                                      options.plotOptions = {
                                          pie: {
                                              allowPointSelect: true,
                                              cursor: 'pointer',
                                              dataLabels: {
                                                  enabled: true,
                                                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                              }
                                          }
                                      }
                                      series[itemNo].data.push({
                                          x: parseInt(item),
                                          y: parseInt(item),
                                          name: items[0]
                                      })
                                  } else {
                                      // Series data - assuming all decimal based numbers
                                      series[itemNo].data.push(parseFloat(item));
                                  }
                              }
                          }); //End loop for processing CSV file
                      }
                  }); //process lines
              } // Process JSON or CSV
              /* Customisations for the context menu */
              if (seriesCounter == sourcesArray.length) {
                  console.log("Drawing chart")
                  var buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems;
                  buttons.splice(8, 1); // Remove the Display Data table option as this gets hidden behind the table in the iframe
                  options.credits = false
                  options.series = series
                  options.chart.renderTo = 'container';
                  interactive = getUrlParameter("interactive")
                  if (!loaded) {
                      if (interactive) {
                          chart = new Highcharts.stockChart(options);
                      } else {
                          chart = new Highcharts.Chart(options);
                      }
                      loaded = true;
                      console.log("Chart has loaded I'll just redraw")
                      //chart.redraw()
                  }
              } else {
                  console.log("> " + seriesCounter + " > " + sourcesArray.length);
              }
          }).fail(function() {
              console.log(" Could not load " + sourceUrl);
              $("#container").html("No data to load from source")
          }); //Get the file
      });
  }
  /*          Highcharts stuff     - no idea if this is needed       */
  /**
   * Display a temporary label on the chart
   */
  function toast(chart, text) {
      chart.toast = chart.renderer.label(text, 100, 120).attr({
          fill: Highcharts.getOptions().colors[0],
          padding: 10,
          r: 5,
          zIndex: 8
      }).css({
          color: '#FFFFFF'
      }).add();
      setTimeout(function() {
          chart.toast.fadeOut();
      }, 2000);
      setTimeout(function() {
          chart.toast = chart.toast.destroy();
      }, 2500);
  }
  /**
   * Custom selection handler that selects points and cancels the default zoom behaviour
   */
  function selectPointsByDrag(e) {
      //                                console.log(e.xAxis)
      //                                console.log(e.xAxis.series[0].groupedData)
      /*if (!chart.selectedLabel) {
          chart.selectedLabel = chart.renderer.label(text, 0, 320)
              .add();
      } else {
          chart.selectedLabel.attr({
              text: text
          });
      }*/
      // Select points
      Highcharts.each(this.series, function(series) {
          Highcharts.each(series.points, function(point) {
              if (point.x >= e.xAxis[0].min && point.x <= e.xAxis[0].max) {
                  //&& point.y >= e.yAxis[0].min && point.y <= e.yAxis[0].max) {
                  point.select(true, true);
              }
          });
      });
      // Fire a custom event
      Highcharts.fireEvent(this, 'selectedpoints', {
          points: this.getSelectedPoints()
      });
      return false; // Don't zoom
  }
  /**
   * The handler for a custom event, fired from selection event
   */
  function selectedPoints(e) {
      // Show a label
      toast(this, '<b>' + e.points.length + ' points selected.</b>' + '<br>Click on empty space to deselect.');
  }
  /**
   * On click, unselect all points
   */
  function unselectByClick() {
      var points = this.getSelectedPoints();
      if (points.length > 0) {
          Highcharts.each(points, function(point) {
              point.select(false);
          });
      }
  }