type = getUrlParameter("type")
sources = getUrlParameter("sources")
title = getUrlParameter("title")
subtitle = getUrlParameter("subtitle")
xTitle = getUrlParameter("xtitle")
yTitle = getUrlParameter("ytitle")
mode = getUrlParameter("mode");
sources = getUrlParameter("sources")
poll = getUrlParameter("poll");
windowSizeMin = 0
windowSizeMax = 0
windowSizeTrigger = 0
loaded = false
chart = ""
window_v = getUrlParameter("window")
chartAttr = ["sources", "window", "poll", "mode", "annotation", "interaction", "button", "type", ];
// This picks up the source URL's and formats them as an API
sources = getUrlParameter("sources") || ""
sourcesArray = []
if (sources.indexOf(",") != -1) {
    //If there's multiple sources to load
    sourcesArray = sources.split(",");
} else {
    //If there's only one
    sourcesArray[0] = sources + (window_v ? window_v : "")
}

function createSettingsFormViewer(attribute) {
    /*
        This generates the settings page and form elements
    */
    data = getParam(attribute)
    $('#' + attribute).show();
    $("#" + attribute).append($('<b/>').text(attribute.substr(0, 1).toUpperCase() + attribute.substr(1)))
    editor = $('<div/>').attr("id", attribute + "-form").css("display", "none").append($('<textarea/>').attr("id", attribute + "-input").attr("cols", 75).attr("rows", 2).text(data).keyup(function(e) {
        if ((e.keyCode || e.which) == 13) { //Enter keycode
            addParam(attribute, $(this).val());
            $('#' + attribute + "-form").hide()
            $('#' + attribute + "-values").show()
        }
    }));
    $('#' + attribute).append(editor);
    if (data != null) {
        $('#' + attribute).append($('<div/>').attr("id", attribute + "-values")).append(function() {
            ul = $('<span/>');
            data.split(",").forEach(function(item) {
                ul.append($('<p/>').text(item));
            });
            return ul
        }).append($('<button/>').attr("id", attribute + "-form-edit").text('edit').click(function() {
            value = getParam(attribute);
            $("#" + attribute + "-input").val(value);
            $('#' + attribute + "-form").show()
            $('#' + attribute + "-values").hide()
        })).show();
    } else {
        editor.show();
    }
}
$(document).ready(function() {
    var mode = getUrlParameter("mode");
    if (mode == "show" && sources != null) {
        $("#setup-button").show();
        $("#chart-button").hide();
        console.log("Initializing chart" + (poll ? ", poll set to " + poll : ""))
        if (poll > 0) {
            setInterval(function() {
                console.log("Reloading chart on poll interval " + poll * 1000)
                loadChart(sourcesArray)
            }, poll * 1000);
        } else {
            loadChart(sourcesArray)
        }
    } else {
        $("#setup-button").hide();
        $("#chart-button").show();
        if (mode == "setup") {
            $("#setup").show();
            $("#loading").hide();
            title = getUrlParameter("title")
            subtitle = getUrlParameter("subtitle")
            xTitle = getUrlParameter("xtitle")
            yTitle = getUrlParameter("ytitle")
            type = getUrlParameter("type")
            mode = getUrlParameter("mode")
            chartAttr.forEach(function(item) {
                createSettingsFormViewer(item);
            });
            console.log("Loaded setup")
        } else {
            console.log("Mode is " + mode)
            location.href = "?mode=setup"
        }
    }
    // Setup mode 
    $('#showButton').click(function() {
        addParam('mode', 'show&');
    });
    $('#setup-button').click(function() {
        console.log("Showing setup")
        addParam('mode', 'setup');
    });
    $('#chart-button').click(function() {
        console.log("Showing widget")
        $("#loading").show();
        addParam('mode', 'show');
    });
    var mode = getUrlParameter("mode");
    sources = getUrlParameter("sources")
    poll = getUrlParameter("poll");
    /* interactive = getUrlParameter("interactive")
     if (interactive) {
         $("#settings").show()
     } else {
         $("#settings").hide()
     }*/
});