/*
 *  C4IOT LTD
 *  Pure JavaScript functions
 */
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
var list = function list(urlJsonArray, callback) {
    $.ajax({
        type: "GET",
        async: false,
        url: urlJsonArray,
        contentType: "application/json",
        dataType: "json",
        success: function(msg) {
            callback(msg)
        },
        error: function(msg) {
            console.log('error : ' + msg.d);
        }
    });
}

function populateSelectOptions(domId, jsonArray) {
    var select = document.getElementById(domId);
    if (select) {
        var option;
        for (var i = 0; i < jsonArray.length; i++) {
            option = document.createElement('option');
            labelExist = jsonArray[i].indexOf("label")
            if (labelExist > -1) {
                node = JSON.parse(jsonArray[i]);
                option.title = jsonArray[i] + "Anddd a"; //Nothing?
                option.value = node.name; //What is sent to the server
                option.text = node.label // What is displayed to the user? 
            } else {
                option.title = jsonArray[i] + "Anddd a"; //Nothing?
                option.value = jsonArray[i]; //What is sent to the server
                option.text = jsonArray[i] // What is displayed to the user? 
            }
            select.add(option);
        }
    }
};

function listenForCommandSeries(clientConfig, topic, appendResponsesToDiv, finishCondition) {
    config = {}
    config.username = 'pleq-stomp-watcher'
    config.password = 'Pl3q123!'
    config.url = "node.pleq.io"
    config.port = 15674
    config.vhost = "staging"
    config.topic = topic
    var on_connect = function(x) {
        id = client.subscribe("/exchange/amq.topic/" + config.topic, function(d) {
            console.log(d)
            //   handleDataToAppend(d)
            $("#response").append("[STOMP] " + JSON.stringify(d) + "<br/>")
        });
    };
    var on_error = function() {
        console.log('error');
    };
    //Create client - clientConfig
    var ws = new SockJS('http://' + config.url + ':15674/stomp');
    var client = Stomp.over(ws);
    client.connect(config.username, config.password, on_connect, on_error, config.vhost);
    //Subscribe - topic
    //onMessage - appendResponsesToDiv
    //whenMessage contains finishCondition kill the client OR after X messages OR after timeout 
}

function dhm(t) {
    var cd = 24 * 60 * 60 * 1000,
        ch = 60 * 60 * 1000,
        d = Math.floor(t / cd),
        h = Math.floor((t - d * cd) / ch),
        m = Math.round((t - d * cd - h * ch) / 60000),
        pad = function(n) {
            return n < 10 ? '0' + n : n;
        };
    if (m === 60) {
        h++;
        m = 0;
    }
    if (h === 24) {
        d++;
        h = 0;
    }
    return [d, pad(h), pad(m)].join(':');
}

function millisecondsToStr(milliseconds) {
    // TIP: to find current time in milliseconds, use:
    // var  current_time_milliseconds = new Date().getTime();
    function numberEnding(number) {
        return (number > 1) ? 's' : '';
    }
    var temp = Math.floor(milliseconds / 1000);
    var years = Math.floor(temp / 31536000);
    if (years) {
        return years + ' year' + numberEnding(years);
    }
    //TODO: Months! Maybe weeks? 
    var days = Math.floor((temp %= 31536000) / 86400);
    if (days) {
        return days + ' day' + numberEnding(days);
    }
    var hours = Math.floor((temp %= 86400) / 3600);
    if (hours) {
        return hours + ' hour' + numberEnding(hours);
    }
    var minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) {
        return minutes + ' minute' + numberEnding(minutes);
    }
    var seconds = temp % 60;
    if (seconds) {
        return seconds + ' second' + numberEnding(seconds);
    }
    return 'less than a second'; //'just now' //or other string you like;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function addParam(param, value) {
    url = location.href;
    if (url.indexOf(param + "=") > -1) {
        //                              (sources=).*?(&)
        //                              (sources=).*?$
        //            url = url.replace(/(sources=).*?(&)/, '$1' + value + '$2');
        var regex = new RegExp("(" + param + "=).*?(&)");
        url = url.replace(regex, '$1' + value + '$2');
        location.href = url;
        /*          var regex = new RegExp("(" + param + "=).*?$");
                  url = url.replace(regex, '$1' + value);
                  location.href = url;*/
        /*   var regex = new RegExp("(?" + param + "=).*?$");
            url = url.replace(regex, '$1' + value);
            location.href = url;
*/
    } else {
        console.log('not found well just add it');
        url = location.href + "&" + param + "=" + value
        location.href = url;
    }
}

function getParam(param) {
    urlParam = getUrlParameter(param)
    setupFieldParam = $("#" + param).val();
    if (!urlParam && setupFieldParam) {
        addParam(param, setupFieldParam);
        urlParam = setupFieldParam
    }
    if (!setupFieldParam && urlParam) {
        $("#" + param).val(urlParam);
        setupFieldParam = urlParam
    }
    return urlParam;
}